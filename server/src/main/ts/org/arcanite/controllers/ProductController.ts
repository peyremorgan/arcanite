import { Request, Response, Application } from 'express';

export default class ProductController
{
	constructor(app: Application)
	{
		app.route('/products')
		   .get(this.findProducts)
		   .post(this.newProduct)
		   .put(this.updateProduct);
	}

	public findProducts(request: Request, response: Response) 
	{
		response.json({});
	}
	
	public newProduct(request: Request, response: Response) 
	{
		response.json({});
	}

	public updateProduct(request: Request, response: Response) 
	{
		response.json({});
	}
}
