import express from 'express'
import ProductController from '~/org/arcanite/controllers/ProductController'

const app = express();
const port = process.env.PORT || 3000;

new ProductController(app);

app.get('/', (req, res) => 
{
	res.send('Hello world');
});
app.listen(port, () => 
{
	return console.log(`server is listening on ${port}`);
});
