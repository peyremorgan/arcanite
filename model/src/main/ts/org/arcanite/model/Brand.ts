import Thing from '~/org/arcanite/model/Thing';

/**
 * A name used for labeling a product group.
 */
export default class Brand extends Thing 
{
	slogan?: string;
}
