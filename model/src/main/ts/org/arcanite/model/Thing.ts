/**
 * The most generic type of item.
 * 
 * Includes intangible things such as Brands and tangible things such as Persons.
 *
 * Canonical URL: http://schema.org/Thing
 */
export default class Thing 
{
	readonly id: string|URL;
	name: string;
	alternateNames: string[];

	description?: Description;
	url?: URL;

	constructor(identifier:string, name?:string, alternateNames?:string[]) 
	{
		this.id = identifier;
		this.name = name || identifier;
		this.alternateNames = alternateNames || [];
	}
}

interface Description 
{
	text: string;
	disambiguation?: string;
}
