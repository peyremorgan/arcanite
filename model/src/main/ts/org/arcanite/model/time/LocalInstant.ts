export default class LocalInstant
{
	/**
	 * The number of nanoseconds between the UNIX epoch and this Instant.
	 */
	timestamp: number;

	/**
	 * The width of the confidence interval.
	 *
	 * e.g. if this Instant represents a given day, precision should be 8.64e13
	 */
	precision?: number;

	constructor(timestamp:number, precision?:number) {
		this.timestamp = timestamp;
		this.precision = precision;
	}
}
