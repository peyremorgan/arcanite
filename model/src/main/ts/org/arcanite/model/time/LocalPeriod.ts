import LocalInstant from './LocalInstant';

export default class LocalPeriod
{
	start: LocalInstant;
	end: LocalInstant;

	constructor(start:LocalInstant, end: LocalInstant)
	{
		this.start = start;
		this.end = end;
	}
}

