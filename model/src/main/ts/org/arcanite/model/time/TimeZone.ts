import Thing from 'org/arcanite/model/Thing'

export default class TimeZone extends Thing
{
	/**
	 * The timezone offset relative to GMT
	 */
	offset: number;

	constructor(offset:number, name?:string)
	{
		super(offset.toString(), name);
		this.offset = offset;
	}
}
