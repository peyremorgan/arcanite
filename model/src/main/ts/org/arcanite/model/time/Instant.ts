import LocalInstant from './LocalInstant';
import TimeZone from './TimeZone';

export default class Instant extends LocalInstant
{
	zone: TimeZone;

	constructor(timestamp:number, zone:TimeZone, precision?:number)
	{
		super(timestamp, precision);
		this.zone = zone;
	}
}
