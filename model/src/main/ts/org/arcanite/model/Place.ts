import Thing from './Thing';

export default class Place extends Thing
{
	location: Location;

	constructor(identifier:string, location:Location, name?:string, alternateNames?:string[]) 
	{
		super(identifier, name, alternateNames);
		this.location = location;
	}
}

class Location
{
	latitude: number;
	longitude: number;
	altitude: number=0;

	constructor(latitude:number, longitude:number, altitude:number=0)
	{
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}
}
