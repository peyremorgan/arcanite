import Price from '~/org/arcanite/model/Price';
import Product from '~/org/arcanite/model/Product';
import Service from '~/org/arcanite/model/Service';
import Thing from '~/org/arcanite/model/Thing';

/**
 * An offer to transfer some rights to an item or to provide a service.
 * 
 * Canonical URL: https://schema.org/Offer
 */
export default class Offer extends Thing 
{
	item: Product|Service

	constructor(identifier: string, item: Product|Service) {
		super(identifier);
		this.item = item;
	}
}
