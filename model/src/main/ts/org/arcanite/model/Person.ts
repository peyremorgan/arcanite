import Thing from '~/org/arcanite/model/Thing';
import Event from '~/org/arcanite/model/Event';
import Place from '~/org/arcanite/model/Place';

/**
 * A person
 *
 * Canonical URL: http://schema.org/Person
 */
export default class Person extends Thing 
{
	address?: Place;
	fax?: string;
	phone?: string;

	birth?: Event;
	death?: Event;

	parents?: Person[];
	children?: Person[];
}
