import MonetaryAmount from '~/org/arcanite/model/MonetaryAmount';
import PaymentMethod from '~/org/arcanite/model/PaymentMethod';

/**
 * The pricing of a Product or Service.
 */
export default class Price
{
	pricing: PriceTier[];

	constructor(defaultPrice: MonetaryAmount, pricing?: PriceTier[])
	{
		if(pricing)
		{
			this.pricing = pricing;
		}
		else
		{
			this.pricing = [];
		}

		this.pricing.push({minimumQuantity:1, unitPrice: defaultPrice});
	}
}

interface PriceTier
{
	minimumQuantity?: number;
	maximumQuantity?: number;
	unitPrice: MonetaryAmount;
	acceptedPaymentMethods?: PaymentMethod[];
}
