import Currency from '~/org/arcanite/model/Currency';

export default class MonetaryAmount
{
	amount: number;
	currency: Currency;

	constructor(amount: number, currency: Currency)
	{
		this.amount = amount;
		this.currency = currency;
	}
}
