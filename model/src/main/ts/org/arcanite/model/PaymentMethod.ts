import Thing from '~/org/arcanite/model/Thing';

export default class PaymentMethod extends Thing
{
}

export const PaymentMethods: {[id:string]: PaymentMethod} =
{
	"CARD": new PaymentMethod("Card"),
	"WIRE": new PaymentMethod("Wire"),
	"CHECK": new PaymentMethod("Check"),
	"COLLECT_ON_DELIVERY": new PaymentMethod("CollectOnDelivery"),
	"DIRECT_DEBIT": new PaymentMethod("DirectDebit")
}
