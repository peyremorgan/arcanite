import Instant from '~/org/arcanite/model/time/Instant';
import Period from '~/org/arcanite/model/time/Period';
import Thing from '~/org/arcanite/model/Thing';
import Place from '~/org/arcanite/model/Place';

export default class Event extends Thing 
{
	time?: Instant | Period
	location?: Place;
	
	isDiscrete():boolean|undefined
	{
		if (this.time instanceof Instant) return true;
		if (this.time instanceof Period)  return false;
		else return undefined;
	}
}
