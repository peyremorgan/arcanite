import Thing from '~/org/arcanite/model/Thing';

/**
 * A service, either offered or already purchased.
 * 
 * For example: A haircut, a print service or the rental of a car.
 */
export default class Service extends Thing 
{
	
}
