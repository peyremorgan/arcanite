import Brand from '~/org/arcanite/model/Brand';
import Thing from '~/org/arcanite/model/Thing';

/**
 * An individual product, either for sale or already puchased.
 * 
 * For example: a pair of shoes, a concert ticket or an individual song download.
 */
export default class Product extends Thing 
{
	brand?: Brand;
	gtin?: string;
	condition?: Condition;

	consumables?: Product[];
}

enum Condition {
	New,
	LikeNew,
	Used,
	Damaged,
	Refurbished
}
